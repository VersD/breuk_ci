#ifndef _HR_BroJZ_Breuk_
#define _HR_BroJZ_Breuk_

typedef struct
{
    int teller;
    int noemer;
} Breuk;

Breuk add(Breuk b1, Breuk b2);
Breuk sub(Breuk b1, Breuk b2);
Breuk mul(Breuk b1, Breuk b2);
Breuk divide(Breuk b1, Breuk b2); // name div is already taken by stdlib

#endif
