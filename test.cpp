#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

#include "src/breuk.h"

TEST_CASE("We gaan breuken testen", "[breuken]")
{
    SECTION("optellen"){
        // Init
        Breuk a = {1, 3}, b = {2, 4};

        // Add 
        Breuk c = add(a, b);
        
        CHECK( c.teller == 5);
        CHECK( c.noemer == 6);
    }

    SECTION("vermenigvuldigen"){
        // Init
        Breuk a = {-1, 3}, b = {2, 4};

        // Add 
        Breuk c = mul(a, b);

        CHECK( c.teller == -1);
        CHECK( c.noemer == 6);
    }

    SECTION("nul"){
        // Init
        Breuk a = { 0, 1}, b = {3, 1};

        // Add
        Breuk c = add(a, b);
        
        // Check results
        CHECK(c.teller == 3);
        CHECK(c.noemer == 1);

        // Multiply
        Breuk d = mul(a, b);

        // Check results
        CHECK(d.teller == 0);
        CHECK(d.noemer == 1);
    } 

}

